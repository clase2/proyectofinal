<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conocen */

$this->title = 'Create Conocen';
$this->params['breadcrumbs'][] = ['label' => 'Conocens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conocen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
