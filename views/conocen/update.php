<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conocen */

$this->title = 'Update Conocen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Conocens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conocen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
