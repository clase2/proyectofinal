<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Viven */

$this->title = 'Create Viven';
$this->params['breadcrumbs'][] = ['label' => 'Vivens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="viven-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
