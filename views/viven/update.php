<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Viven */

$this->title = 'Update Viven: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vivens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="viven-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
