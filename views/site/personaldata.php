<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Registros';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">
    <br/>
    <div class="jumbotron">
        <h1>Registros</h1>

        <p class="lead">Sistema de consultas</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-6 col-md-4">
               <div class="thumbnail">
                  <div class="caption">
                    <h2>Usuarios</h2>

                    <p><small>Visualización de la tabla usuarios</small></p>
                    <p>
                        <?= Html::a('Usuarios',['/usuarios/index'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('Listas',['/site/usuarios'],['class'=>'btn btn-default'])?>
                    </p>
                  </div>
               </div>
            </div>
            <div class="col-sm-6 col-md-4">
               <div class="thumbnail">
                  <div class="caption">
                    <h2>Personas</h2>

                    <p><small>Visualización de la tabla personas</small></p>
                    <p>
                        <?= Html::a('Personas',['/personas/index'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('Listas',['/site/personas'],['class'=>'btn btn-default'])?>
                    </p>
                  </div>
               </div>
            </div>
            <div class="col-sm-6 col-md-4">
               <div class="thumbnail">
                  <div class="caption">
                    <h2>Conocen</h2>

                    <p><small>Visualización de la tabla conocen</small></p>
                    <p>
                        <?= Html::a('Conocen',['/conocen/index'],['class'=>'btn btn-primary'])?>
                    </p>
                  </div>
               </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
               <div class="thumbnail">
                  <div class="caption">
                    <h2>Números</h2>

                    <p><small>Visualización de la tabla numeros</small></p>
                    <p>
                        <?= Html::a('Números',['/numeros/index'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('Listas',['/site/numeros'],['class'=>'btn btn-default'])?>
                    </p>
                  </div>
               </div>
            </div>
            <div class="col-sm-6 col-md-4">
               <div class="thumbnail">
                  <div class="caption">
                    <h2>Direcciones</h2>

                    <p><small>Visualización de la tabla direcciones</small></p>
                    <p>
                        <?= Html::a('Direcciones',['/direcciones/index'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('Listas',['/site/direcciones'],['class'=>'btn btn-default'])?>
                    </p>
                  </div>
               </div>
            </div>
            <div class="col-sm-6 col-md-4">
               <div class="thumbnail">
                  <div class="caption">
                    <h2>Viven</h2>

                    <p><small>Visualización de la tabla viven</small></p>
                    <p>
                        <?= Html::a('Viven',['/viven/index'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('Listas',['/site/viven'],['class'=>'btn btn-default'])?>
                    </p>
                  </div>
               </div>
            </div>
        </div>
    </div>
</div>

