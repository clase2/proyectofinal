<?php
/* @var $this yii\web\View */

$this->title = 'SecureUser';
?>
<div class="row">
    <div class="hidden-xs col-md-4 panel panel-primary">
        <div class="panel-heading">
          <h5>Por que usarlo</h5>
        </div>
        <div class="panel-body">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h5>Sencillez</h5>
                </div>
                <div class="panel-body">
                    <p>Es tan sencillo como descargarte los módulos e implementarlos en tu aplicación web.</p>
                </div>
            </div>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h5>Encriptación</h5>
                </div>
                <div class="panel-body">
                    <p>Gracias a la securización y encriptación de los datos podrá 
                        estar seguro de que la información más valiosa está a salvo.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="site-index ">
            <div class="jumbotron">
                <h1>Bienvenido!</h1>
                <h2>Sistema base de gestión de usuarios</h2>
                <p><a class="btn btn-lg btn-success" href="./site/login">Acceda a su cuenta</a></p>
                <h3><spam>Defina <strong>AHORA</strong> la seguridad de su empresa.</spam></h3>
            </div>

            <div class="body-content">
                 <div class="container-fluid">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Que es  SecureUser</div>
                        <div class="panel-body">
                            <strong>Secureuser</strong> es un Gestor <strong>Seguro</strong> de usuarios 
                            dedicado a empresas que desean almacenar de una manera segura 
                            gran cantidad de usuarios y poder acceder a sus registros de manera 
                            segura y sencilla.
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>



