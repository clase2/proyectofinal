<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Tablas adicionales';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">
    <br/>
    <div class="jumbotron">
        <h1>Tablas adicionales</h1>

        <p class="lead">Sistema de consultas</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-6 col-md-4">
               <div class="thumbnail">
                  <div class="caption">
                    <h2>Provincias</h2>

                    <p><small>Visualización de la tabla provincias</small></p>
                    <p>
                        <?= Html::a('Listas',['/site/provincias'],['class'=>'btn btn-default'])?>
                    </p>
                  </div>
               </div>
            </div>
            <div class="col-sm-6 col-md-4">
               <div class="thumbnail">
                  <div class="caption">
                    <h2>Poblaciones</h2>

                    <p><small>Visualización de la tabla poblaciones</small></p>
                    <p>
                        <?= Html::a('Listas',['/site/poblaciones'],['class'=>'btn btn-default'])?>
                    </p>
                  </div>
               </div>
            </div>
           <div class="col-sm-6 col-md-4">
               <div class="thumbnail">
                  <div class="caption">
                    <h2>Preguntas</h2>

                    <p><small>Visualización de la tabla preguntas</small></p>
                    <p>
                        <?= Html::a('Preguntas',['/preguntas/index'],['class'=>'btn btn-primary'])?>
                    </p>
                  </div>
               </div>
            </div>
        </div>        
    </div>
</div>


