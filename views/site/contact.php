<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contacta con nosotros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <div class="panel-body panel panel-success">
                <div class="panel-heading">
                    <h1><?= Html::encode($this->title) ?></h1>
                </div>
                <div class="panel-body">
                    <p>Si lo desea podrá hacer uso de los siguientes contactos ya sea para responderle 
                    alguna duda o para solucionarle algún percance.</p>
                    <div class="panel panel-danger" style="margin:3%; text-align: center">
                        <div class="panel-heading">
                            <h4>Importante</h4>
                        </div>
                        <div class="pannel-body">
                            <p>Las contestaciones pueden tardar algunos días,<br/> por favor tenga paciencia.</p>
                        </div>
                    </div>
                    <div class="panel-body panel panel-primary">
                        <div class="panel-heading"><h3><u>Contacto</u></h3></div>
                        <div class="pannel-body">
                                <br/>
                                <div>Tlfn: <spam>665-02-83-33</spam></div>
                                <div>Email: <a href="mailto:bryansanchezafonso@gmail.com"><spam>bryansanchezafonso@gmail.com</spam></a></div>
                                <div>Página web: <a href="https://bryansanchezafonso.000webhostapp.com"><button class="btn btn-top">Web</button></a></div>
                                <br/>
                                <div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d853.7873046775139!2d-3.8229249971267683!3d43.46166781457414!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd494bd44c740213%3A0xfc2a7294e63988ac!2sCalle%20Cardenal%20Cisneros%2C%20111%2C%2039007%20Santander%2C%20Cantabria!5e1!3m2!1ses!2ses!4v1580057882640!5m2!1ses!2ses" width="100%" height="100%" frameborder="0" style="border:0;border-radius:10px;" allowfullscreen=""></iframe></div>
                        </div>
                    </div>
                </div>                
            </div>
</div>
