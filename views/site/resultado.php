<?php

use yii\grid\GridView;
?>

<div>
    <h2>
        <?=$titulo?>
    </h2>
</div>
<div>
    <a href="<?=$ruta?>"><button class="btn btn-success">Modificar tabla <?=$tabla?></button></a>
</div>
<br/>

    <?= GridView::widget([
     'dataProvider'=>$resultados,
    ]);?>
