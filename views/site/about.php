<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Documentación';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-2 panel panel-primary">
        <div class="panel-heading">
                    <h4>En el documento</h4>
        </div>
        <div class="panel-body">
            <ul class="list-group lista">
                <li class="list-group-item"><a href="#d1">Que es SecureUser</a></li>
                <li class="list-group-item"><a href="#d2">Instalación y acceso al administrador de Yii</a></li>
                <li class="list-group-item"><a href="#d3">Tráfico de datos y encriptación</a></li>
                <li class="list-group-item"><a href="#d4">Código</a></li>
            </ul>
            
        </div>
    </div>
    <div class="col-md-10">
        <div class="site-about panel panel-primary"  style="padding: 4%">
            <div class="panel-heading">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="panel-body panel panel-primary" id="d1">
                <div class="panel-heading">
                    <h3>Que es SecureUser</h3>
                </div>
                <div class="panel-body">
                    <p>SecureUser es un gestor de usuarios creado en PHP.
                    La parte del administrador ha sido enteramente desarrollada sobre Yii.
                    <br/>SecureUser dota de seguridad a cualquier aplicación Web la cual haga uso de administración de usuarios.</p>                    
                </div>                
            </div>
            <div class="panel-body panel panel-success" id="d2">
                <div class="panel-heading">
                    <h3>Instalación y acceso al administrador de Yii</h3>
                </div>
                <div class="panel-body">
                    <h3><u>Instalación</u></h3>
                    <p>Instale xampp y composer.</p>
                    <p>Una vez instalado acceda a la carpeta htdocs/"su-proyecto"/ y cree la carpeta users, dentro de esta y desde su terminal de git corra el siguiente comando.</p>
                    <p>Ejecute el siguiente comando desde la consola de Git dentro de la carpeta yii.</p>
                    <small><code>git clone https://gitlab.com/clase2/proyectofinal.git</code></small>
                    <p>Acceda a la carpeta data y corra el archivo .sql sobre su servidor de MySQL.</p>
                    <div class="panel panel-danger" style="margin:3%; text-align: center">
                        <div class="panel-heading">
                            <h4>Importante</h4>
                        </div>
                        <div class="pannel-body">
                            <p>Para que Yii pueda correr,la rama en git debe ser la<strong> master</strong>.<br/>
                            Si la modifica, este no correrá.</p>
                        </div>
                    </div>
                    <h3><u>Acceso</u></h3>
                    <p>Para acceder solo debe entrar en su buscador y acceder a la carpeta <code>http://"server"/"su-proyecto"/</code>.</p>
                    <p>Para tener acceso al administrador solo debe loguearse en la pestaña de login.</p>
                    <div class="panel-body panel panel-warning">
                        <div class="panel-heading">
                            <h3>Editar las credenciales de los usuarios al gestor</h3>
                            <p><small>"Se recomienda eliminar las credenciales de admin y demo"</small></p>
                        </div>
                        <div class="panel-body">
                            <p>Para editar las credenciales de usuario entre al archivo <code>"/su-proyecto"/models/User.php</code></p>
                            <pre>
private static $users = [                                  _
        '100' => [                                          |
            'id' => '100',                                  |
            'username' => 'admin',       ======>   <strong><u>Nombre</u></strong>   |
            'password' => 'admin',       ======> <strong><u>Contraseña</u></strong> | <strong><u>Usuario 1</u></strong>
            'authKey' => 'test100key',                      |
            'accessToken' => '100-token',                   |
        ],                                                 _|
                                                           _
        '101' => [                                          |
            'id' => '101',                                  |
            'username' => 'demo',                           |
            'password' => 'demo',                           | <strong><u>Usuario 2</u></strong>
            'authKey' => 'test101key',                      |
            'accessToken' => '101-token',                   |
        ],                                                 _|
];
                            </pre>                            
                        </div>
                    </div>                    
                </div>                
            </div>
            <div class="panel-body panel panel-danger" id="d3">
                <div class="panel-heading">
                    <h3>Tráfico de datos y encriptación</h3>
                </div>
                <div class="panel-body">
                    <h3><u>Tráfico de datos</u></h3>
                    <p>Se recomiendan instalar el aplicativo
                    en una máquina independiente</p>
                    <h4><u>Contiene</u></h4>  
                    <ul>
                        <li><h5><u>En esta versión</u></h5> 
                            <ul>
                                <li>Encriptación por SSL</li>
                                <li>Gestor PHP para la I/O de los datos</li>
                            </ul>
                        </li>
                        <li><h5><u>En la siguiente versión</u></h5> 
                            <ul>
                                <li>Encriptación por SSL</li>
                                <li>Encriptación entre el servidor y la DB</li>
                                <li>Gestor PHP para la I/O de los datos</li>
                            </ul>
                        </li>
                    </ul>
                    <div class="panel-body panel panel-success">
                        <div class="panel-heading">
                            <h3>Funciones PHP para el acceso a los datos</h3>
                        </div>
                        <div class="panel-body">
                            <h3><u>Funciones</u></h3>
                            <p>Descripción y uso de las diferentes funciones.</p>
                            <div class="panel-body panel panel-warning">
                                <div class="panel-heading">
                                    <h3>Uso de las funciones PHP para el acceso a los datos</h3>
                                </div>
                                <div class="panel-body">
                                    <p>Para utilizar las funciones instale los archivos php situado en la carpeta <strong>/data/funciones</strong> del gestor.</p>
                                    <p>Para instalar el archivo de funciones siga estos dos sencillos pasos:</p>
                                    <ul>
                                        <li>Cree la carpeta <strong>php</strong> de la siguiente manera: <code>"carpeta de su proyecto web"/<strong>php/</strong></code></li>
                                        <li>Copie los archivo de <strong>/data/funciones</strong> php en la carpeta anteriormente creada</li>
                                        <li>Modifique el archivo <strong>/data/funciones/db.json</strong> ingresando los valores de conexión a su base de datos <small>(la misma sobre la que corren los datos gestionados por Yii)</small>.</li>
                                    </ul>
                                </div>
                            </div>
                            <h3><u>Funciones</u></h3>
                            <ul>
                            <li><p>El archivo dbconn.php crea una conexión segura con la db con lo que se reduce el tiempo de programación</p></li>
                            <h3>Dentro de funciones.php</h3>
                            <li><p><strong>function salidaDatos($query):</strong><br/>Retorna un array(array("nombre de columnas"),array("resultados por columnas"))<br/>Destinada principalmente para select</p></li>
                            <li><p><strong>function entrada($query):</strong> Entradas de $query a base de datos. Retorna "Error" si no se realiza la query</p></li>  
                            <li><p><strong>function comprobarConn():</strong> Retorna el estado de la conexión con la db.</p></li>  
                            <ul>
                        </div>                
                    </div>
                </div>                
            </div>
            <div class="panel-body panel panel-warning" id="d4">
                <div class="panel-heading">
                    <h3>Código</h3>
                </div>
                <div class="panel-body">
                    <p><strong>Licencia: </strong><u>CC0 1.0 Universal (CC0 1.0)
                            Ofrecimiento al Dominio Público</u></p>
                    <div class="panel panel-danger" style="margin:3%; text-align: center">
                        <div class="panel-heading">
                            <h4>Importante</h4>
                        </div>
                        <div class="pannel-body">
                            <p>Este proyecto es puramente libre y gratuito, por favor respete e intente 
                                no comercializar productos a partir de este proyecto.</p>
                        </div>
                    </div>
                    <div class="panel panel-success" style="margin:3%; text-align: center">
                        <div class="panel-heading">
                            <h4>Componentes</h4>
                        </div>
                        <div class="pannel-body">
                            <h4><u>Lenguajes</u></h4>
                            <ul>
                                <li>PHP</li>
                                <li>JavaScripts</li>
                            </ul>
                            <h4><u>Entorno</u></h4>
                            <ul>
                                <li>Yii Framework</li>
                                <li>Composer</li>
                                <li>Xammp server</li>
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>
