<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Numeros */

$this->title = 'Create Numeros';
$this->params['breadcrumbs'][] = ['label' => 'Numeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="numeros-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
