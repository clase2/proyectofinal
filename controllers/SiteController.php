<?php

namespace app\controllers;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuarios;
use app\models\Personas;
use app\models\Conocen;
use app\models\Direcciones;
use app\models\Numeros;
use app\models\Poblaciones;
use app\models\Preguntas;
use app\models\Provincias;
use app\models\Viven;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login','logout','personaldata','otherdata'],
                'rules' => [
                    [
                        'actions' => ['login','logout','personaldata','otherdata'],
                        'allow' => true,
                        'roles' => ['@'],
                        'denyCallback' => function ($rule, $action) {
                                                 throw new \Exception('Ya está logueado');
                                           }
                    
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionPersonaldata()
    {
        return $this->render('personaldata');
    }
    public function actionOtherdata()
    {
        return $this->render('otherdata');
    }
    
    public function actionUsuarios()
    {
        $query = new Query();
        $query ->select('login,password,ruta_img,id_preguntas,pregunta,respuesta')->from('usuarios');
        $query ->join('INNER JOIN','preguntas','usuarios.id_preguntas = preguntas.id');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('resultado', [
            "resultados" => $dataProvider,
            "titulo"=>"Listado de todos los usuarios",
            "ruta"=>"../usuarios/index",
            "tabla"=>"Usuarios"
        ]);
    }
     public function actionPersonas()
    {
        $query = new Query();
        $query ->select('DNI,nombre,apellido1,apellido2,fecha_nac,idioma,nacionalidad,login')->from('personas');
        $query ->join('INNER JOIN','usuarios','personas.id_usuario = usuarios.id');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('resultado', [
            "resultados" => $dataProvider,
            "titulo"=>"Listado de todas las personas",
            "ruta"=>"../personas/index",
            "tabla"=>"Personas"
        ]);
    }
    public function actionNumeros()
    {
        $query = new Query();
        $query ->select('DNI,nombre,num AS numero,esMovil')->from('numeros');
        $query ->join('INNER JOIN', 'personas', 'numeros.id_persona=personas.DNI');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('resultado', [
            "resultados" => $dataProvider,
            "titulo"=>"Listado de la Agenda",
            "ruta"=>"../numeros/index",
            "tabla"=>"Números"
        ]);
    }
     public function actionProvincias()
    {
        $query = new Query();
        $query ->select('*')->from('provincias');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('resultado2', [
            "resultados" => $dataProvider,
            "titulo"=>"Províncias de España",
            
            "tabla"=>"Provincias"
        ]);
    }
     public function actionPoblaciones()
    {
        $query = new Query();
        $query ->select('*')->from('poblaciones');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('resultado2', [
            "resultados" => $dataProvider,
            "titulo"=>"Poblaciones de España",           
            "tabla"=>"Provincias"
        ]);
    }
    public function actionDirecciones()
    {
        $query = new Query();
        $query ->select('id_poblacion,poblaciones.nombre AS poblacion,direcciones.nombre as nombre,numero,CP,comentarios')->from('direcciones');
        $query ->join('INNER JOIN', 'poblaciones', 'direcciones.id_poblacion=poblaciones.id');        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('resultado', [
            "resultados" => $dataProvider,
            "titulo"=>"Listado de direcciones",
            "ruta"=>"../direcciones/index",
            "tabla"=>"Provincias"
        ]);
    }
    public function actionViven()
    {
        $query = new Query();
        $query ->select('id_persona,personas.nombre AS Nombre,apellido1,apellido2,id_direccion,direcciones.nombre AS Direccion,numero,CP,comentarios')->from('viven');
        $query ->join('INNER JOIN', 'personas', 'viven.id_persona=personas.DNI');        
        $query ->join('INNER JOIN', 'direcciones', 'viven.id_direccion=direcciones.id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('resultado', [
            "resultados" => $dataProvider,
            "titulo"=>"Listado de direcciones",
            "ruta"=>"../viven/index",
            "tabla"=>"Provincias"
        ]);
    }
    
}
