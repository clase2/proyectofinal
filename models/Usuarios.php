<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $ruta_img
 * @property int $id_preguntas
 * @property string $respuesta
 *
 * @property Personas[] $personas
 * @property Preguntas $preguntas
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password', 'ruta_img', 'id_preguntas', 'respuesta'], 'required'],
            [['id_preguntas'], 'integer'],
            [['login', 'respuesta'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 12],
            [['ruta_img'], 'string', 'max' => 50],
            [['login'], 'unique'],
            [['ruta_img'], 'unique'],
            [['id_preguntas'], 'exist', 'skipOnError' => true, 'targetClass' => Preguntas::className(), 'targetAttribute' => ['id_preguntas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'ruta_img' => 'Ruta Img',
            'id_preguntas' => 'Id Preguntas',
            'respuesta' => 'Respuesta',
        ];
    }

    /**
     * Gets query for [[Personas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Personas::className(), ['id_usuario' => 'id']);
    }

    /**
     * Gets query for [[Preguntas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreguntas()
    {
        return $this->hasOne(Preguntas::className(), ['id' => 'id_preguntas']);
    }
}
