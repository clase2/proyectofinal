<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "poblaciones".
 *
 * @property int $id
 * @property int $id_provincia
 * @property string $nombre
 *
 * @property Direcciones[] $direcciones
 * @property Provincias $provincia
 */
class Poblaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'poblaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_provincia', 'nombre'], 'required'],
            [['id_provincia'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['nombre'], 'unique'],
            [['id_provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['id_provincia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_provincia' => 'Id Provincia',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Direcciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirecciones()
    {
        return $this->hasMany(Direcciones::className(), ['id_poblacion' => 'id']);
    }

    /**
     * Gets query for [[Provincia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia()
    {
        return $this->hasOne(Provincias::className(), ['id' => 'id_provincia']);
    }
}
