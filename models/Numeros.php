<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "numeros".
 *
 * @property int $id
 * @property string $id_persona
 * @property int $num
 * @property int $esMovil
 *
 * @property Personas $persona
 */
class Numeros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'numeros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_persona', 'num', 'esMovil'], 'required'],
            [['num', 'esMovil'], 'integer'],
            [['id_persona'], 'string', 'max' => 9],
            [['num'], 'unique'],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => Personas::className(), 'targetAttribute' => ['id_persona' => 'DNI']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_persona' => 'Id Persona',
            'num' => 'Num',
            'esMovil' => 'Es Movil',
        ];
    }

    /**
     * Gets query for [[Persona]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Personas::className(), ['DNI' => 'id_persona']);
    }
}
