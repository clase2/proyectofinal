<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conocen".
 *
 * @property int $id
 * @property string|null $id_conoce
 * @property string|null $id_conocido
 *
 * @property Personas $conoce
 * @property Personas $conocido
 */
class Conocen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conocen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_conoce', 'id_conocido'], 'string', 'max' => 9],
            [['id_conoce'], 'exist', 'skipOnError' => true, 'targetClass' => Personas::className(), 'targetAttribute' => ['id_conoce' => 'DNI']],
            [['id_conocido'], 'exist', 'skipOnError' => true, 'targetClass' => Personas::className(), 'targetAttribute' => ['id_conocido' => 'DNI']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_conoce' => 'Id Conoce',
            'id_conocido' => 'Id Conocido',
        ];
    }

    /**
     * Gets query for [[Conoce]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConoce()
    {
        return $this->hasOne(Personas::className(), ['DNI' => 'id_conoce']);
    }

    /**
     * Gets query for [[Conocido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConocido()
    {
        return $this->hasOne(Personas::className(), ['DNI' => 'id_conocido']);
    }
}
