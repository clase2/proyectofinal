<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personas".
 *
 * @property string $DNI
 * @property string $nombre
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fecha_nac
 * @property string $idioma
 * @property string $nacionalidad
 * @property int $id_usuario
 *
 * @property Conocen[] $conocens
 * @property Conocen[] $conocens0
 * @property Numeros[] $numeros
 * @property Usuarios $usuario
 * @property Viven[] $vivens
 */
class Personas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DNI', 'nombre', 'apellido1', 'apellido2', 'fecha_nac', 'idioma', 'nacionalidad', 'id_usuario'], 'required'],
            [['fecha_nac'], 'safe'],
            [['id_usuario'], 'integer'],
            [['DNI'], 'string', 'max' => 9],
            [['nombre', 'apellido1', 'apellido2', 'idioma', 'nacionalidad'], 'string', 'max' => 20],
            [['DNI'], 'unique'],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DNI' => 'Dni',
            'nombre' => 'Nombre',
            'apellido1' => 'Apellido1',
            'apellido2' => 'Apellido2',
            'fecha_nac' => 'Fecha Nac',
            'idioma' => 'Idioma',
            'nacionalidad' => 'Nacionalidad',
            'id_usuario' => 'Id Usuario',
        ];
    }

    /**
     * Gets query for [[Conocens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConocens()
    {
        return $this->hasMany(Conocen::className(), ['id_conoce' => 'DNI']);
    }

    /**
     * Gets query for [[Conocens0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConocens0()
    {
        return $this->hasMany(Conocen::className(), ['id_conocido' => 'DNI']);
    }

    /**
     * Gets query for [[Numeros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumeros()
    {
        return $this->hasMany(Numeros::className(), ['id_persona' => 'DNI']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'id_usuario']);
    }

    /**
     * Gets query for [[Vivens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVivens()
    {
        return $this->hasMany(Viven::className(), ['id_persona' => 'DNI']);
    }
}
