<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "viven".
 *
 * @property int $id
 * @property string $id_persona
 * @property int $id_direccion
 *
 * @property Personas $persona
 * @property Direcciones $direccion
 */
class Viven extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'viven';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_persona', 'id_direccion'], 'required'],
            [['id_direccion'], 'integer'],
            [['id_persona'], 'string', 'max' => 9],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => Personas::className(), 'targetAttribute' => ['id_persona' => 'DNI']],
            [['id_direccion'], 'exist', 'skipOnError' => true, 'targetClass' => Direcciones::className(), 'targetAttribute' => ['id_direccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_persona' => 'Id Persona',
            'id_direccion' => 'Id Direccion',
        ];
    }

    /**
     * Gets query for [[Persona]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Personas::className(), ['DNI' => 'id_persona']);
    }

    /**
     * Gets query for [[Direccion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDireccion()
    {
        return $this->hasOne(Direcciones::className(), ['id' => 'id_direccion']);
    }
}
