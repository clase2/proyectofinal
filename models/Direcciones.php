<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "direcciones".
 *
 * @property int $id
 * @property int $id_poblacion
 * @property string $nombre
 * @property int|null $numero
 * @property int $CP
 * @property string|null $comentarios
 *
 * @property Poblaciones $poblacion
 * @property Viven[] $vivens
 */
class Direcciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'direcciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_poblacion', 'nombre', 'CP'], 'required'],
            [['id_poblacion', 'numero', 'CP'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['comentarios'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
            [['id_poblacion'], 'exist', 'skipOnError' => true, 'targetClass' => Poblaciones::className(), 'targetAttribute' => ['id_poblacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_poblacion' => 'Id Poblacion',
            'nombre' => 'Nombre',
            'numero' => 'Numero',
            'CP' => 'Cp',
            'comentarios' => 'Comentarios',
        ];
    }

    /**
     * Gets query for [[Poblacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPoblacion()
    {
        return $this->hasOne(Poblaciones::className(), ['id' => 'id_poblacion']);
    }

    /**
     * Gets query for [[Vivens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVivens()
    {
        return $this->hasMany(Viven::className(), ['id_direccion' => 'id']);
    }
}
