<?php

$data = file_get_contents("db.json");
$items = json_decode($data, true);


$ip = $items["database"]["ip"];
$port = $items["database"]["port"];
$dbname = $items["database"]["dbname"];
$user = $items["database"]["user"]["user"];
$password = $items["database"]["user"]["password"];

$enlace = mysqli_connect($ip,$user,$password,$dbname,$port);

if (!$enlace) {
    echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
    echo "errno de depuración: " . mysqli_connect_errno() . PHP_EOL;
    exit;
}
?>
