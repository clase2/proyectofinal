<?php

include 'dbconn.php';

function salidaDatos($query){

    $result= $enlace->query($query);

    $info_campo = $result->fetch_fields();

        $coll = array();
        $resultados = array();

        foreach ($info_campo as $valor) {
            $coll[]=$valor->name;
        }

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                foreach($coll as $columnas){
                    $resultados[]=$row[$columnas];
                }
            }
        }

    return array($coll,$resultados);    
    $resultado->close();
}

function entrada($query){
    if (!$enlace->query($query)) {
        return $query."Error";
    }
}

function comprobarConn(){
    if(!$enlace){return "DB desconectada";}
    else{return "DB conectada";}    
}

$enlace->close();
?>