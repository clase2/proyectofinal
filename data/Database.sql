﻿DROP DATABASE SecureUser;
CREATE DATABASE SecureUser;
USE SecureUser;
#Creamos tablas
CREATE TABLE preguntas(id int(6) PRIMARY KEY AUTO_INCREMENT,pregunta varchar(20) NOT NULL UNIQUE);
CREATE TABLE usuarios(id int(6) PRIMARY KEY auto_increment, login varchar(50) NOT NULL UNIQUE,password varchar(12) NOT NULL, ruta_img varchar(50) NOT NULL UNIQUE,id_preguntas int(6) NOT NULL,respuesta varchar(20) NOT null);
CREATE TABLE personas(DNI char(9) PRIMARY KEY, nombre varchar(20) NOT NULL, apellido1 varchar(20) NOT NULL, apellido2 varchar(20) NOT NULL,fecha_nac date NOT NULL, idioma varchar(20) NOT NULL, nacionalidad varchar(20) NOT NULL,id_usuario int(6) NOT NULL);
CREATE TABLE conocen(id int(4) PRIMARY KEY AUTO_INCREMENT,id_conoce char(9),id_conocido char(9));
CREATE TABLE numeros(id int(10) PRIMARY KEY auto_increment,id_persona char(9) NOT null,num int(9) NOT NULL UNIQUE,esMovil boolean NOT NULL);
CREATE TABLE provincias(id int(3) PRIMARY KEY AUTO_INCREMENT, nombre varchar(20) NOT NULL UNIQUE);
CREATE TABLE poblaciones(id int(4) PRIMARY KEY AUTO_INCREMENT,id_provincia int(3) NOT NULL,nombre varchar(20) NOT NULL UNIQUE);
CREATE TABLE direcciones(id int(6) PRIMARY KEY AUTO_INCREMENT,id_poblacion int(4) NOT NULL,nombre varchar(100) NOT NULL UNIQUE,numero int(5),CP int(5) NOT NULL,comentarios varchar(100));
CREATE TABLE viven  (id int(6) PRIMARY KEY AUTO_INCREMENT,id_persona char(9) NOT NULL, id_direccion int(6) NOT NULL);
#Creamos index
ALTER TABLE conocen ADD INDEX id_conoce (id_conoce,id_conocido);
ALTER TABLE poblaciones ADD INDEX idx_provincia (id_provincia);
#Creamos foreign key
ALTER TABLE usuarios ADD FOREIGN KEY (id_preguntas) REFERENCES preguntas(id);
ALTER TABLE personas ADD FOREIGN KEY (id_usuario) REFERENCES usuarios(id);
ALTER TABLE conocen ADD FOREIGN KEY (id_conoce) REFERENCES personas(DNI);
ALTER TABLE conocen ADD FOREIGN KEY (id_conocido) REFERENCES personas(DNI);
ALTER TABLE numeros ADD FOREIGN KEY (id_persona) REFERENCES personas(DNI);
ALTER TABLE poblaciones ADD FOREIGN KEY (id_provincia) REFERENCES provincias(id);
ALTER TABLE direcciones ADD FOREIGN KEY (id_poblacion) REFERENCES poblaciones(id);
ALTER TABLE viven ADD FOREIGN KEY (id_persona) REFERENCES personas(DNI);
ALTER TABLE viven ADD FOREIGN KEY (id_direccion) REFERENCES direcciones(id);